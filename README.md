# Documentarian

_A module for scaffolding, writing, previewing, and generating documentation_

This PowerShell module provides plaster templates, helper functions, and build scripts for the following:

- Scaffolding documentation
- Building and previewing documentation
- Adding new documents
- Managing GitBook Plugins

Future updates will add new helpers, templates, and documentation.
This early release targets documentation split into the [three types](http://michaeltlombardi.gitlab.io/needful-docs/) of documentation and utilizing GitBook for the static site generation.

## Install

You can download a copy of this project's source code locally or install from the gallery.

### Install From Source
You can [download the latest code from source](https://gitlab.com/documentarian/documentarian/repository/master/archive.zip) as a zip file.
Unzip it somewhere on your local drive - I use `C:/code/` for all of my projects.

Open a PowerShell prompt at the root of the unzipped project with administrator privileges, then run the following command:

```powershell
./build.ps1
```

That will run through the default task list for this project, installing the module.

### Install From the PowerShell Gallery

The following line of code will, presuming you have [PowerShellGet](https://docs.microsoft.com/en-us/powershell/gallery/psget/get_psget_module) installed on your computer, install the module to your machine.
It will only install the module for the current user and so the install does not require administrator privileges.

```powershell
Find-Module -Name documentarian -Repository PSGallery | Install-Module -Force -Scope CurrentUser
```

Note: If you have never downloaded or installed a module with PowerShellGet you may be asked to install NuGet, a dependency for PowerShellGet.
You will need to agree to that download and install to continue.
Alternatively you can download the module's source code and [follow those installation instructions](#install-from-source).

### Install Optional Prerequisite Software

The tool we are using with this project for documentation has a few prerequisites.
These prerequisites are only required if you want to build and view documentation locally.
In order to build and preview the documentation you will need to have [NodeJS](https://nodejs.org/en/) and [NPM](https://www.npmjs.com/get-npm) installed.
In order to build the documentation as a PDF you will need to have [Calibre](https://calibre-ebook.com/) installed.

To install NodeJS and NPM:

```powershell
# On Windows, if using chocolatey:
choco install nodejs npm
# On MacOS, if using homebrew:
brew install nodejs npm
# On Ubuntu:
sudo apt-get install nodejs npm
# On Fedora 24+:
sudo dnf install nodejs
# On Fedora <=23:
sudo dnf install nodejs npm
# On Arch Linux:
sudo pacman -S nodejs npm
```

```powershell
# On Windows, if using chocolatey:
choco install calibre
# On MacOS, if using homebrew:
brew cask install calibre
# On Linux:
sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.py | sudo python -c "import sys; main=lambda:sys.stderr.write('Download failed\n'); exec(sys.stdin.read()); main()"
```

## Use

Open a PowerShell prompt in the folder you want to add docs to.

```powershell
Import-Module documentarian
Invoke-Plaster -TemplatePath $GitBookPlasterTemplate -DestinationPath .
```

You'll be prompted for the name of your project, a brief description, and an author - then the documentation and related files will be scaffolded out for you!

If you'd like to compile your documentation locally, ensure you have the [prerequisite software installed](#install-optional-prerequisite-software).
Then navigate to the root folder of the project you're working on and open a PowerShell prompt:

```powershell
./build.ps1 -TaskList GenerateSite,GeneratePdf
```

That will output your documentation into a folder for the static site (`/public` under your root folder) and into a pdf file named after your project.
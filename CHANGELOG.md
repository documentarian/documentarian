# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.10.1] 20180-03-20
### Changed
+ Clarified the project's pitch, function, and value in the readme and the manifest description.

## [0.10.0] 20180-03-20
### Fixed
+ Re-implemented the `Add-GitBookPlugin` and `Remove-GitBookPlugin` functions.
+ Ensure reference documentation is actually being published to the public docs site.

## [0.9.1] 2018-03-16
### Added
+ Published the module to the [gallery](https://www.powershellgallery.com/packages/documentarian/0.9.1).

### Changed
+ Moved the exportable variables into their own folder and out of the PSM1.
+ Ensured the module will build the functions and variables into a single PSM1.
  The module files list is now a PSD1, a PSM1, and the folder(s) of plaster templates.
+ Improved the readability and usability of the README.
+ Updated the manifest in preparation for publishing to the gallery.

## [0.9.0] 2018-02-27
### Added
+ `New-Document` function to scaffold out markdown document files in a project.

### Changed
+ Separated the tasks for building gitbook docs from the primary psake file in both the template and the project.

## [0.8.0] 2017-06-21
### Added
+ Functions for adding (`Add-GitBookPlugin`) and removing (`Remove-GitBookPlugin`) GitBook plugins using PowerShell instead of manually editing your GitBook config files
+ Narrative documentation for including media files in your documentation
+ Functionality for including the `html5-video` GitBook plugin with your scaffolded GitBook documentation if you need to support audio/video files
+ Functionality in both the template and project psake tasks for `Compile` to copy the media folders of a project into the compiling folder if one is found

## [0.7.0] 2017-06-09
### Added
+ Added option to specify copyright holder name in the plaster template.
This is used for the copyright notice in the license.

## [0.6.2] 2017-06-09
### Fixed
+ Added variables for the output site path and output pdf path to the psake file.
This ensures that the `Clean` task can run successfully.

## [0.6.1] 2017-06-09
### Fixed
+ Ensured that the `book.json` and `.bookignore` files are scaffolded into the `docs` folder instead of the project root.
Those files weren't being copied into the compiling folder when building the documentation.
+ Removed mention of the GitBook GUI editor, since it assumes that a project _is_ a gitbook, not that a project is _using_ gitbook for documentation.
The gitbook editor will fail to load on this project layout.

## [0.6.0] 2017-06-09
### Changed
+ Updated the project documentation and template to be more clear about the types of documentation that are being scaffolded.

## [0.5.0] 2017-06-07
### Added
+ Exported variable `$GitBookPlasterTemplate` from the documentarian module.
This will let users specify the variable instead of having to find the path to the template themselves every time they need to run it.
### Changed
+ Updated the readme to reflect using the variable and importing the module instead of saving off a fragile path in a variable, which they have previously needed to do every session.

## [0.4.0] 2017-06-07
### Added
+ LivePreview task for both the project and the template, enabling users to get a live preview of their gitbook docs running locally.

## [0.3.0] 2017-06-07
### Added
+ book.json for the project docs, which had been accidentally lost due to gitignore settings, which are now fixed.
### Changed
+ Made the compilation of documentation for gitbook safer in both the template and for the project itself.
The documentation will now compile into a temporary folder in the project root.
We've also added a `clean` task so that you can be sure you're always building your documentation without extra artifacts.
+ We've modified the gitignore the project creates for this new layout.


## [0.2.0] 2017-06-07
### Changed
+ Ensured that the build scripts do not need to always run with elevated permissions.
Added warnings to the psake task files for chocolatey package installations, ensuring that the tasks check for admin permissions and fail clearly if those permissions aren't present.

## [0.1.0] 2017-06-06
### Added
+ Documentation scaffolding.
+ Initial build for installing prerequisites, adding module to PSModule path, and compiling documentation (both as a site and as a PDF)
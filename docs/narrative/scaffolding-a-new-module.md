# Scaffolding a New Module with Plaster and Documentarian

This document will walk you through scaffolding out (that is, creating the files and folders for) a new PowerShell module using the `Plaster` and `Documentarian` modules.
We'll walk you through the following steps:

1. [Installing Plaster and Documentarian](#installing-plaster-and-documentarian)
2. [Scaffolding out the module itself](#scaffolding-the-module)
3. [Scaffolding out documentation for the module](#scaffolding-documentation)
4. [Previewing the default documentation](#previewing-the-documentation)
5. [Adding a document](#adding-a-document)
6. [Exporting the documentation as static site and PDF](#exporting-the-documentation)

**NOTE:** This walkthrough assumes you're using PowerShell 5.1 or higher.

## Installing Plaster and Documentarian

PowerShell 5.1 and higher include the [`PowerShellGet`](https://github.com/powershell/powershellget) module by default, meaning that we can install modules from the [PowerShell Gallery](https://powershellgallery.com).

We're going to install both of these modules into the `CurrentUser` scope, meaning they are only installed for the person who runs this command.
Installing modules this way does not require administrator permissions.

```powershell
# If you have not already installed a PowerShell module using PowerShellGet before,
# you'll need to run this command to ensure the correct prerequisites are in place:
Install-PackageProvider -Name 'PowerShellGet' -Scope CurrentUser -Force -ForceBootstrap
# Here we install the modules themselves from the public gallery:
Install-Module -Name 'Plaster', 'Documentarian' -Scope CurrentUser -Repository PSGallery -Force
```

Having run the above commands, you should now have both modules installed locally.
You can verify this by running `Get-Module`:

```powershell
Get-Module -ListAvailable -Name 'Plaster', 'documentarian'
```

If that command results in an error or otherwise does not include output for both modules, something has gone wrong.
Please reach out to us or file an issue and we'll do what we can to help!

Assuming everything went as planned, we're ready to scaffold out our new PowerShell module.

## Scaffolding the Module

Now that we have our modules installed and ready for use we can scaffold out our module.
We'll use the default _PowerShell Manifest Module_ template from Plaster.

First, we want to make sure we're at our home directory (or another safe path to work from, if you like) for the rest of this walkthrough - we'll be creating files & folders!

```powershell
# If you want to work from a different path, change this line;
# We'll be using $WorkingPath for the rest of this walkthrough.
$WorkingPath = '~'
Push-Location -Path $WorkingPath
```

Once we're in the right location we need to import the modules for use:

```powershell
Import-Module -Name 'Plaster','documentarian'
```

Next, we need to retrieve the path to the plaster template.
Unfortunately, there's not an elegant way to get the Plaster module template right now; this pipeline of code will search for the template and assign the path to a variable.

```powershell
$ModuleTemplatePath = Get-PlasterTemplate `
| Where-Object -FilterScript {$PSItem.Title -eq 'New PowerShell Manifest Module' -and $PSItem.Author -eq 'Plaster'} `
| Select-Object -ExpandProperty TemplatePath
```

Now that we have stored the template path in a variable, we're ready to create our module!
For this walkthrough, we're going to call our module `MyExample`.

```powershell
Invoke-Plaster -TemplatePath $ModuleTemplatePath -DestinationPath "$WorkingPath/MyExample"
```

When you use `Invoke-Plaster` to scaffold out the project it will ask you a few questions.

+ The name of the module is `MyExample`.
+ The version is `0.0.1` (you can hit enter to accept the default).
+ This walkthrough assumes you're using Visual Studio Code as your editor, so you can hit enter to accept the default.
+ If you're using a different editor, specify `N` for none.

Once you've finished answering the questions Plaster will scaffold the module for you, creating the necessary folders and files.
You can verify that those files and folders were created by running `Get-ChildItem`:

```powershell
Get-ChildItem -Path "$WorkingPath/MyExample" -Recurse
```

The default plaster template scaffolds out a module file, a manifest, and a folder for tests.
Notably missing, however, are the documents and folders for documentation.
This is where the documentarian module can help us.

## Scaffolding Documentation

Now that we've scaffolded out the basis of our new module it is time to scaffold out our documentation.
We've provided a variable, `$GitBookPlasterTemplate` to make it easier for you to use the template at the prompt.
As before, we invoke plaster and supply the path to the template as well as the path to our module:

```powershell
Invoke-Plaster -TemplatePath $GitBookPlasterTemplate -DestinationPath "$WorkingPath/MyExample"
```

Again, plaster will ask us a few questions:

+ The name of the project is `MyExample`.
+ You can use any short description you like for the module; we used `An example module with documentation`
+ You can put yourself as the primary author for the project, or any other name you like.
+ You can use the same name for who has rights to the project.
+ For this example, our documentation _does_ need to support audio/video files.

Once again, plaster scaffolds out files and folders for us.
We can verify this by rerunning the `Get-ChildItem` command:

```powershell
Get-ChildItem -Path "$WorkingPath/MyExample" -Recurse
```

Note that there are many more files and folders than before we invoked the plaster template.

## Previewing the Documentation

**Note:** This step assumes that you have both NodeJS and NPM installed.
If you do not, please [make sure those applications are installed on your local machine](../README.md#install-optional-prerequisite-software).

Before we add or change any of the documentation we will want to go ahead and preview the docs site as it renders by default.
We can do this by running the build script in the root folder of our project and specifying the task 'LivePreview'.

```powershell
Push-Location -Path "$WorkingPath/MyExample"
./build.ps1 -TaskList LivePreview
```

This will download the required NodeJS packages and start a live preview of the documentation which you can then [view in your browser](http://localhost:4000/) (this link will only work once you have started the live preview).

From here you can navigate around.
Make sure to take a look at the scaffolding for [concept](http://localhost:4000/concept/readme.html), [narrative](http://localhost:4000/narrative/readme.html), and [reference](http://localhost:4000/reference/readme.html) documentation, as well as the [changelog](http://localhost:4000/CHANGELOG.html) and [license](http://localhost:4000/LICENSE.html).

You can also search the docs site from the top-left-hand-corner; try searching on 'patterns best practices'.
This should bring up the Concept documentation page as a result - the search bar is able to do a full text search on all included documentation.

In the terminal where you started the live preview you can type `ctrl+C` to break out of the preview.

## Adding a Document

Now that we have all of the prerequisites available and have been able to preview our site it is time to add a new document.
For this example, we're going to add a concept document titled 'When To Use MyExample'.

In the PowerShell prompt at the root folder of our project:

```powershell
New-Document -Title 'When To Use MyExample' -Type Concept
./build.ps1 -TaskList LivePreview
```

The commands above will create the new document in the correct folder and then start the live preview of the documentation site.
Once the live preview is up, open the [preview of the new document](http://localhost:4000/concept/when-to-use-myexample.html).

Next, open the new document in your editor - you can find it at `MyExample/docs/Concept/when-to-use-myexample.md` where `MyExample` is the root folder of your project.
You can write whatever you like in this file - here's the example text we chose:

```markdown
# When To Use MyExample

You should use the MyExample module whenever you want to solve SomeProblem.
The MyExample module is an excellent choice if one or more of the following apply to you:

+ You meet condition one
+ You do not meet condition two
+ You have one of the following requirements:
  + first
  + second
  + third
+ You have used MyOtherProject, but experienced SomeIssue using it.

You may not want to use the MyExample module if one or more of the following apply to you:

+ You meet condition two
+ You are using MyOtherProject without any issues
+ You have to maintain compatibility with LegacyProject.

As is often the case, there's not a good one-size-fits-all answer for whether or not MyExample is a good fit for you or your team.
The choice will depend on your particular context.
```

Save the file after updating it.
All going according to plan, the live preview page _should_ update.
If it does not do so automatically, try refreshing the page.
If that does not update the page, cancel the live preview by pressing `ctrl+C` in the terminal and restart it.

You should be able to see the changes you have made to the new document in the browser.

## Exporting the Documentation

Once we are happy with our documentation we can export that documentation into a static site.
This document won't get into _deploying_ the generated documentation, but we will briefly cover exporting the documentation.

Again in the PowerShell prompt at the root folder of the project:

```powershell
./build.ps1 -TaskList GenerateSite
```

This command will compile the documentation and generate all of the required files for the static site, exporting those files to the `public` folder in the project root.
The tree of the generated project should then look something like this:

```text
./public/
├── CHANGELOG.html
├── LICENSE.html
├── concept
│   ├── readme.html
│   └── when-to-use-myexample.html
├── gitbook
│   ├── fonts
│   │   └── fontawesome
│   │       ├── FontAwesome.otf
│   │       ├── fontawesome-webfont.eot
│   │       ├── fontawesome-webfont.svg
│   │       ├── fontawesome-webfont.ttf
│   │       ├── fontawesome-webfont.woff
│   │       └── fontawesome-webfont.woff2
│   ├── gitbook-plugin-expandable-chapters-small
│   │   ├── expandable-chapters-small.css
│   │   └── expandable-chapters-small.js
│   ├── gitbook-plugin-fontsettings
│   │   ├── fontsettings.js
│   │   └── website.css
│   ├── gitbook-plugin-highlight
│   │   ├── ebook.css
│   │   └── website.css
│   ├── gitbook-plugin-lunr
│   │   ├── lunr.min.js
│   │   └── search-lunr.js
│   ├── gitbook-plugin-search
│   │   ├── lunr.min.js
│   │   ├── search-engine.js
│   │   ├── search.css
│   │   └── search.js
│   ├── gitbook-plugin-sharing
│   │   └── buttons.js
│   ├── gitbook.js
│   ├── images
│   │   ├── apple-touch-icon-precomposed-152.png
│   │   └── favicon.ico
│   ├── style.css
│   └── theme.js
├── index.html
├── narrative
│   └── readme.html
├── reference
│   └── readme.html
└── search_index.json
```

### Bonus: Exporting as PDF

**Note:** This step assumes that you have both Calibre installed.
If you do not, please [make sure those applications are installed on your local machine](../README.md#install-optional-prerequisite-software).

In addition to exporting your documentation in a format that can be hosted as a static site we can also export our documentation as a PDF file.
That PDF can then be uploaded into all sorts of knowledge bases or artifact repositories which do not support static sites.
This is a pretty good way to write documentation once and have it available in multiple formats/places.

To export the documentation as a PDF type the following at a PowerShell prompt in the root of the project folder:

```powershell
./build.ps1 -TaskList GeneratePDF
```

In the case of this example, that task will create `MyExample.pdf` in the root of the project.
You can then open that file to preview it prior to upload.

## Conclusion

This document provided a walkthrough of the current features of the documentarian module in an effort to help you get started.
We walked through installing the module, scaffolding a new project, previewing the documentation, adding new documents, and generating the publishable artifacts from source.

If you ran into any problems not resolved in this document, or have any questions, please reach out to us via [email](mailto:incoming+documentarian/documentarian@gitlab.com), [twitter](https://twitter.com/barbariankb), or by [filing an issue](https://gitlab.com/documentarian/documentarian/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

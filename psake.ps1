#!/usr/bin/env pwsh
# Include tasks for documentation
Include $PSScriptRoot/docs.psake.ps1
# PSake makes variables declared here available in other scriptblocks
# Init some things
Properties {
    $ProjectRoot       = $env:BHProjectPath
    $Timestamp         = Get-date -uformat "%Y%m%d-%H%M%S"
    $ApiKey            = $env:APIKEY
    $BuildFolder       = "$ProjectRoot/$env:BHProjectName"
}

Task Default -Depends InstallModule

Task CleanBuild {
    If (Test-Path -Path $BuildFolder) {
        Remove-Item -Path $BuildFolder -Recurse -Force
    }
}

Task BuildModule -depends CleanBuild {
    $null = New-Item -Path $BuildFolder -ItemType Directory -Force
    # Copy over the manifest without changes
    Copy-Item -Path "$ProjectRoot/source/$ENV:BHProjectName.psd1" -Destination $BuildFolder -Force
    # Construct the PSM1:
    Get-ChildItem "$ProjectRoot/source/variables", "$ProjectRoot/source/functions" -Recurse -File `
    | Select-Object -ExpandProperty FullName `
    | ForEach-Object -Process {
        $Content = Get-Content -Path $PSItem
        Add-Content -Path "$BuildFolder/$ENV:BHProjectName.psm1" -Value $Content
    }
    Add-Content -Path "$BuildFolder/$ENV:BHProjectName.psm1" -Value 'Export-ModuleMember -Variable * -Function * -Alias *'
    # Copy the template folders:
    Copy-Item -Path $ProjectRoot/source/gitbook -Destination $BuildFolder -Container -Recurse -Force
}

Task InstallModule -depends BuildModule, DeterminePlatform {
    # This will install the latest version of the module.
    $Version = Get-Module $ProjectRoot/Source/documentarian.psd1 -ListAvailable | Select-Object -ExpandProperty Version
    # Detect the location to install the module to ; by default, install into the user scope.
    If ($Script:Platform -eq 'Windows') {
        $ModuleRoot = $env:PSModulePath.split(';')[0]
    } Else {
        $ModuleRoot = $env:PSModulePath.split(':')[0]
    }
    $InstallFolder = "$ModuleRoot/documentarian/$Version"
    If (!(Test-Path -Path $InstallFolder)) {
        New-Item -Path "$ModuleRoot/documentarian/$Version" -ItemType Directory -Force
    }
    Copy-Item -Path $BuildFolder/*       -Destination "$ModuleRoot/documentarian/$Version" -Force
    Copy-Item -Path $BuildFolder/gitbook -Destination "$ModuleRoot/documentarian/$Version" -Container -Recurse -Force
}

Task PublishModule -depends BuildModule {
    Publish-Module -Path "$BuildFolder" -Repository PSGallery -NugetApiKey $env:APIKEY -Verbose
}
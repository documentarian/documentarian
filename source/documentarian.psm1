ForEach ($Folder in 'functions','variables') {
  Get-ChildItem -Path $PSScriptRoot/functions -Recurse -File `
  | ForEach-Object -Process {. $PSItem.FullName}
}

Export-ModuleMember -Variable * -Function * -Alias *